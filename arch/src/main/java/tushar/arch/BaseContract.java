package tushar.arch;

import android.arch.lifecycle.Lifecycle;
import android.os.Bundle;

/**
 * Created by TUSHAR on 2/16/18.
 * tsaha.cse@gmail.com
 */

public interface BaseContract {

    interface View {

        void showLoading();

        void hideLoading();

        void showToast(String message);
    }

    interface Presenter<V extends BaseContract.View> {

        void attachLifecycle(Lifecycle lifecycle);

        void detachLifecycle(Lifecycle lifecycle);

        void attachView(V view);

        boolean isViewAttached();

        V getView();

        Bundle getStateBundle();

        void detachView();

        void onPresenterCreated();

        void onPresenterDestroy();

        void onViewAttached();

        void onViewDetached();
    }
}

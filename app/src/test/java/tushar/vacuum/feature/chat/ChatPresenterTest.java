package tushar.vacuum.feature.chat;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Named;

import io.reactivex.Scheduler;
import tushar.vacuum.di.RunOn;
import tushar.vacuum.repository.DataRepository;
import tushar.vacuum.session.Session;
import tushar.vacuum.validation.Validation;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.atLeastOnce;
import static tushar.vacuum.di.SchedulerType.IO;
import static tushar.vacuum.di.SchedulerType.UI;

/**
 * Created by TUSHAR on 2/23/18.
 * tsaha.cse@gmail.com
 */

public class ChatPresenterTest {

    @Named("shared_preference")
    @Mock
    Session session;

    @Mock
    Validation validation;

    @Mock
    DataRepository dataRepository;

    @Mock
    @RunOn(IO)
    Scheduler ioScheduler;

    @Mock
    @RunOn(UI)
    Scheduler uiScheduler;

    @Mock
    private ChatContract.ChatView view;

    private ChatPresenter presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        presenter = new ChatPresenter(dataRepository, ioScheduler, uiScheduler, session);
        presenter.attachView(view);
    }

    @Test
    public void viewAttached_whenAlreadyLoggedIn_shouldOpenChatActivity() {
        // given
        given(session.isLoggedIn()).willReturn(false);
        presenter.onPresent();
        // Then
        then(view).should(atLeastOnce()).onLogout();
    }
}

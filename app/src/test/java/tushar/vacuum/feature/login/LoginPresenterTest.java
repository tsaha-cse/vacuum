package tushar.vacuum.feature.login;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Named;

import tushar.vacuum.session.Session;
import tushar.vacuum.validation.Validation;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.atLeastOnce;

/**
 * Created by TUSHAR on 2/23/18.
 * tsaha.cse@gmail.com
 */

public class LoginPresenterTest {

    @Named("shared_preference")
    @Mock
    Session session;

    @Mock
    Validation validation;

    @Mock
    private LoginContract.LoginView view;

    private LoginPresenter presenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        presenter = new LoginPresenter(session, validation);
        presenter.attachView(view);
    }

    @Test
    public void viewAttached_whenAlreadyLoggedIn_shouldOpenChatActivity() {
        // given
        given(session.isLoggedIn()).willReturn(true);
        presenter.onViewAttached();
        // Then
        then(view).should(atLeastOnce()).gotoChatActivity();
    }
}

package tushar.vacuum.validation;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by TUSHAR on 2/23/18.
 * tsaha.cse@gmail.com
 */

public class ValidationTest {

    Validation validation;

    @Before
    public void setup() {
        validation = new ValidationImpl();
    }

    @Test
    public void isNameValid_ShouldReturnTrue() {
        assertTrue(validation.isNameValid("Tushar Saha"));
        assertTrue(validation.isNameValid("T S"));
        assertTrue(validation.isNameValid("Juan Calderon Zumba"));
    }

    @Test
    public void isNameValid_ShouldReturnFalse() {
        assertFalse(validation.isNameValid(""));
        assertFalse(validation.isNameValid("TusharSaha"));
        // Last name not included
        assertFalse(validation.isNameValid("Juan"));
        // Last name not included and ended with whitespace
        assertFalse(validation.isNameValid("Tushar "));
    }

    @Test
    public void isNameAvailableForSignup_ShouldReturnFalse() {
        assertFalse(validation.isNameAvailableForSignUp("Rodney"));
        // reserved name ended with a white space
        assertFalse(validation.isNameAvailableForSignUp("Oliva "));
    }

    @Test
    public void isNameAvailableForSignUp_ShouldReturnTrue() {
        assertTrue(validation.isNameAvailableForSignUp("Rodney Oliva"));
        assertTrue(validation.isNameAvailableForSignUp("Oliva Rodney"));
    }
}

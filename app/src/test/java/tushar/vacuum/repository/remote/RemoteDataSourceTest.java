package tushar.vacuum.repository.remote;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Flowable;
import tushar.vacuum.api.ApiService;
import tushar.vacuum.api.ChatResponse;
import tushar.vacuum.model.Chat;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

/**
 * Created by TUSHAR on 2/23/18.
 * tsaha.cse@gmail.com
 */

public class RemoteDataSourceTest {

    private RemoteDataSource remoteDataSource;

    @Mock
    ApiService apiService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        remoteDataSource = new RemoteDataSource(apiService);
    }

    @Test
    public void loadChats_ShouldReturnFromRemoteService() {
        ChatResponse chatResponse = new ChatResponse();
        given(apiService.getChat()).willReturn(Flowable.just(chatResponse));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void addChats_NoThingToDoWithRemoteSource() {
        Chat chat = mock(Chat.class);
        remoteDataSource.addData(chat);
        // should not have to do anything with ApiService
        then(apiService).shouldHaveZeroInteractions();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void clearChats_NoThingToDoWithRemoteSource() {
        remoteDataSource.clearData();
        // should not have to do anything with ApiService
        then(apiService).shouldHaveZeroInteractions();
    }
}

package tushar.vacuum.repository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;
import tushar.vacuum.model.Chat;

import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Created by TUSHAR on 2/23/18.
 * tsaha.cse@gmail.com
 */

public class DataRepositoryTest {

    @Mock
    @Local
    private DataSource localDataSource;
    @Mock
    @Remote
    private DataSource remoteDataSource;

    private DataRepository dataRepository;
    private TestSubscriber<List<Chat>> chatsTestSubscriber;

    private static final Chat chat1 = new Chat();
    private static final Chat chat2 = new Chat();
    private static final Chat chat3 = new Chat();
    private static final List<Chat> chats = Arrays.asList(chat1, chat2, chat3);

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        dataRepository = new DataRepository(localDataSource, remoteDataSource);
        chatsTestSubscriber = new TestSubscriber<>();
    }

    @Test
    public void loadChats_ShouldReturnCache_IfItIsAvailable() {
        // Given cache
        dataRepository.cache.addAll(chats);
        dataRepository.loadData(false).subscribe(chatsTestSubscriber);
        // verify that there is nothing to do
        // with local or remote sources
        verifyZeroInteractions(localDataSource);
        verifyZeroInteractions(remoteDataSource);
        chatsTestSubscriber.assertValue(chats);
    }

    @Test
    public void loadChats_ShouldReturnFromLocal_IfCacheIsUnavailable() {
        doReturn(Flowable.just(chats)).when(localDataSource).loadData(false);
        doReturn(Flowable.just(chats)).when(remoteDataSource).loadData(true);
        dataRepository.loadData(false).subscribe(chatsTestSubscriber);
        // Loads from local source
        verify(localDataSource).loadData(false);
        // Loads from remote source if local data is unavailable
        verify(remoteDataSource).loadData(true);
        chatsTestSubscriber.assertValue(chats);
    }

    @Test
    public void loadChats_ShouldReturnFromRemote_WhenNeeded() {
        doReturn(Flowable.just(chats)).when(remoteDataSource).loadData(true);
        // Force remote
        dataRepository.loadData(true).subscribe(chatsTestSubscriber);
        // Remote get called
        verify(remoteDataSource).loadData(true);
        // Local never called
        verify(localDataSource, never()).loadData(true);
        // Local data are cleared
        verify(localDataSource).clearData();
        // Caches are same as given chats
        assertEquals(dataRepository.cache, chats);
        // Local's add data get called
        verify(localDataSource, times(3)).addData(chat1);
        verify(localDataSource).addData(chat2);
        verify(localDataSource).addData(chat3);
        chatsTestSubscriber.assertValue(chats);
    }

    @Test
    public void clearChats_ShouldClearCachesAndLocalData() {
        // Given cache
        dataRepository.cache.addAll(chats);
        dataRepository.clearData();
        // Cache should be empty
        assertThat(dataRepository.cache, empty());
        // Local data get empty
        then(localDataSource).should().clearData();
    }
}

package tushar.vacuum.repository.local;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;
import tushar.vacuum.model.Chat;
import tushar.vacuum.repository.local.db.ChatDao;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

/**
 * Created by TUSHAR on 2/23/18.
 * tsaha.cse@gmail.com
 */

public class LocalDataSourceTest {

    private LocalDataSource localDataSource;

    @Mock
    ChatDao chatDao;

    private static final Chat chat1 = new Chat();
    private static final Chat chat2 = new Chat();
    private static final Chat chat3 = new Chat();
    private static final List<Chat> chats = Arrays.asList(chat1, chat2, chat3);

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        localDataSource = new LocalDataSource(chatDao);
    }

    @Test
    public void loadChats_ShouldReturnFromDatabase() {
        TestSubscriber<List<Chat>> subscriber = new TestSubscriber<>();
        given(chatDao.getAllChat()).willReturn(Flowable.just(chats));
        localDataSource.loadData(false).subscribe(subscriber);
        // should called
        then(chatDao).should().getAllChat();
    }

    @Test
    public void addChats_ShouldInsertToDatabase() {
        Chat chat = new Chat();
        localDataSource.addData(chat);
        // should called
        then(chatDao).should().insert(chat);
    }

    @Test
    public void clearChats_ShouldDeleteAllDataInDatabase() {
        localDataSource.clearData();
        // should called
        then(chatDao).should().deleteAll();
    }
}

package tushar.vacuum.feature.login;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import tushar.vacuum.R;
import tushar.vacuum.di.login.LoginModule;
import tushar.vacuum.feature.base.BaseExtensionActivity;
import tushar.vacuum.feature.chat.ChatActivity;

public class LoginActivity extends BaseExtensionActivity<LoginContract.LoginView, LoginContract.LoginPresenter> implements LoginContract.LoginView {

    @BindView(R.id.tiName)
    TextInputLayout tiName;

    @BindView(R.id.etName)
    TextInputEditText etName;

    @Inject
    LoginPresenter presenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void init() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected LoginPresenter initPresenter() {
        app.getAppComponent()
                .plus(new LoginModule())
                .inject(this);
        return presenter;
    }

    @Override
    protected void retainPresenter(LoginContract.LoginPresenter presenter) {
        this.presenter = (LoginPresenter) presenter;
    }

    @OnFocusChange(R.id.etName)
    public void onFocusChanged(boolean isFocused) {
        if (isFocused) {
            etName.setHint(getString(R.string.enter_your_full_name));
        } else {
            etName.setHint(null);
        }
    }

    @OnTextChanged(value = R.id.etName, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onTextChanged(CharSequence text) {
        presenter.onTextChanged(text);
    }

    @Override
    public void gotoChatActivity() {
        startActivity(new Intent(this, ChatActivity.class));
        finish();
    }

    @Override
    public String getFullName() {
        return etName.getText().toString().trim();
    }

    @Override
    public void showErrorFullNameIsNull() {
        tiName.setError(getString(R.string.enter_your_full_name));
    }

    @Override
    public void showErrorInvalidFullName() {
        tiName.setError(getString(R.string.invalid_full_name));
    }

    @Override
    public void showErrorFullNameAlreadyTaken() {
        tiName.setError(getString(R.string.full_name_is_taken));
    }

    @Override
    public void hideError() {
        if (!presenter.isEmpty(tiName.getError())) {
            tiName.setError(null);
        }
    }

    @OnClick(R.id.btnLogin)
    void onClickLogin() {
        presenter.onClickLogin();
    }
}

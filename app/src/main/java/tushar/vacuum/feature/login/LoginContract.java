package tushar.vacuum.feature.login;

import tushar.arch.BaseContract;

/**
 * Created by TUSHAR on 2/22/18.
 * tsaha.cse@gmail.com
 */

public interface LoginContract {

    interface LoginView extends BaseContract.View {
        void gotoChatActivity();

        String getFullName();

        void showErrorFullNameIsNull();

        void showErrorInvalidFullName();

        void showErrorFullNameAlreadyTaken();

        void hideError();
    }

    interface LoginPresenter extends BaseContract.Presenter<LoginView> {

        void onClickLogin();

        void onTextChanged(CharSequence text);

        boolean isEmpty(CharSequence error);
    }
}

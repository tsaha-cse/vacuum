package tushar.vacuum.feature.login;

import javax.inject.Inject;
import javax.inject.Named;

import tushar.arch.BasePresenter;
import tushar.vacuum.model.User;
import tushar.vacuum.session.Session;
import tushar.vacuum.validation.Validation;

/**
 * Created by TUSHAR on 2/22/18.
 * tsaha.cse@gmail.com
 */

public class LoginPresenter extends BasePresenter<LoginContract.LoginView> implements LoginContract.LoginPresenter {

    private final Session session;
    private Validation validation;

    @Inject
    public LoginPresenter(@Named("shared_preference") Session session, Validation validation) {
        this.session = session;
        this.validation = validation;
    }

    @Override
    public void onViewAttached() {
        if (session.isLoggedIn()) {
            getView().gotoChatActivity();
        }
    }

    @Override
    public void onViewDetached() {
        // no-op
    }

    @Override
    public void onClickLogin() {
        if (isFullNameOkay()) {
            saveUser(getView().getFullName());
        }
    }

    @Override
    public void onTextChanged(CharSequence text) {
        if (!validation.isNameAvailableForSignUp(text.toString().trim())) {
            getView().showErrorFullNameAlreadyTaken();
        } else {
            getView().hideError();
        }
    }

    @Override
    public boolean isEmpty(CharSequence error) {
        return validation.isNullOrEmpty(error);
    }

    private boolean isFullNameOkay() {
        if (validation.isEmpty(getView().getFullName())) {
            getView().showErrorFullNameIsNull();
            return false;
        }

        if (!validation.isNameValid(getView().getFullName())) {
            getView().showErrorInvalidFullName();
            return false;
        }

        if (!validation.isNameAvailableForSignUp(getView().getFullName())) {
            getView().showErrorFullNameAlreadyTaken();
            return false;
        }

        getView().hideError();

        return true;
    }

    private void saveUser(String fullName) {
        session.saveUser(User.build(fullName));
        getView().gotoChatActivity();
    }
}

package tushar.vacuum.feature.chat;

import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import tushar.vacuum.R;
import tushar.vacuum.di.chat.ChatModule;
import tushar.vacuum.feature.base.BaseExtensionActivity;
import tushar.vacuum.feature.login.LoginActivity;
import tushar.vacuum.feature.util.SoftKeyboardStateWatcher;
import tushar.vacuum.model.Chat;

/**
 * Created by TUSHAR on 2/19/18.
 * tsaha.cse@gmail.com
 */

public class ChatActivity extends BaseExtensionActivity<ChatContract.ChatView, ChatContract.ChatPresenter>
        implements ChatContract.ChatView {

    @BindView(R.id.rvChat)
    RecyclerView rvChat;

    @BindView(R.id.tvInfo)
    TextView tvInfo;

    @BindView(R.id.etMessage)
    EditText etMessage;

    @BindView(R.id.btnSend)
    Button btnSend;

    private ChatAdapter adapter;

    @Inject
    ChatPresenter presenter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_chat;
    }

    @Override
    protected void init() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(presenter.getUserName());
            getSupportActionBar().setSubtitle(getString(R.string.app_name));
        }
        adapter = new ChatAdapter(this, new ArrayList<>());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvChat.setLayoutManager(layoutManager);
        rvChat.setAdapter(adapter);
        rvChat.setItemAnimator(new DefaultItemAnimator());
        adapter.setItemClickListener(position -> showToast(adapter.getItem(position).getUsername()));

        SoftKeyboardStateWatcher softKeyboardStateWatcher = new SoftKeyboardStateWatcher(findViewById(R.id.root));
        softKeyboardStateWatcher.addSoftKeyboardStateListener(new SoftKeyboardStateWatcher.SoftKeyboardStateListener() {
            @Override
            public void onSoftKeyboardOpened(int keyboardHeightInPx) {
                if (adapter.getItemCount() > 0) {
                    rvChat.scrollToPosition(adapter.getItemCount() - 1);
                }
            }

            @Override
            public void onSoftKeyboardClosed() {
                if (adapter.getItemCount() > 0) {
                    rvChat.scrollToPosition(adapter.getItemCount() - 1);
                }
            }
        });
    }

    @Override
    protected ChatContract.ChatPresenter initPresenter() {
        app.getAppComponent()
                .plus(new ChatModule())
                .inject(this);
        return presenter;
    }

    @Override
    protected void retainPresenter(ChatContract.ChatPresenter presenter) {
        this.presenter = (ChatPresenter) presenter;
    }

    @Override
    public void setResult(List<Chat> chats) {
        tvInfo.setVisibility(View.GONE);
        adapter.clear();
        adapter.addAll(chats);
        rvChat.smoothScrollToPosition(adapter.getItemCount() - 1);
    }

    @Override
    public void insertResult(Chat chat) {
        adapter.add(chat);
        etMessage.setText(null);
        rvChat.scrollToPosition(adapter.getItemCount() - 1);
    }

    @Override
    public void onLogout() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @OnTextChanged(value = R.id.etMessage, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void onMessageTextChanged(CharSequence message) {
        btnSend.setEnabled(message.toString().trim().length() > 0);
    }

    @OnClick(R.id.btnSend)
    void onClickSendMessage() {
        presenter.onClickSendMessage(etMessage.getText().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_log_out) {
            presenter.logout();
        }
        return super.onOptionsItemSelected(item);
    }
}

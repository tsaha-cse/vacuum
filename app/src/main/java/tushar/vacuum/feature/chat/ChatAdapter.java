package tushar.vacuum.feature.chat;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.security.InvalidParameterException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.annotations.NonNull;
import tushar.vacuum.R;
import tushar.vacuum.feature.base.BaseRecyclerViewAdapter;
import tushar.vacuum.model.Chat;
import tushar.vacuum.model.UserType;

class ChatAdapter extends BaseRecyclerViewAdapter<Chat, RecyclerView.ViewHolder> {

    private final List<Chat> chats;
    private final Context context;
    private OnItemClickListener clickListener;

    public ChatAdapter(Context context, @NonNull List<Chat> chats) {
        super(chats);
        this.context = context;
        this.chats = chats;
    }

    public void setItemClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return chats.get(position).getUserType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == UserType.OTHER.getType()) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_chat_other, viewGroup, false);
            return new ChatOtherViewHolder(view);
        } else {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_chat, viewGroup, false);
            return new ChatOwnViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ChatOtherViewHolder) {
            ((ChatOtherViewHolder) holder).bind(position);
        } else {
            ((ChatOwnViewHolder) holder).bind(position);
        }
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    public Chat getItem(int position) {
        if (position < 0 || position >= chats.size()) {
            throw new InvalidParameterException("Invalid item index");
        }
        return chats.get(position);
    }

    class ChatOtherViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvMessage)
        TextView titleMessage;

        @BindView(R.id.tvInfo)
        TextView tvInfo;

        @BindView(R.id.imgAvatar)
        RoundedImageView imgAvatar;

        public ChatOtherViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(final int position) {
            Chat chat = chats.get(position);
            titleMessage.setText(chat.getContent().trim());
            tvInfo.setText(context.getString(R.string.chat_info, chat.getUsername(), formatRelativeTime(chat.getTime())));

            if (!TextUtils.isEmpty(chat.getUserImageUrl().trim())) {
                Picasso.with(context)
                        .load(chat.getUserImageUrl())
                        .placeholder(R.color.chat_other)
                        .error(ContextCompat.getDrawable(context, R.color.chat_other))
                        .into(imgAvatar);
            }
        }
    }

    class ChatOwnViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvMessage)
        TextView titleMessage;

        @BindView(R.id.tvInfo)
        TextView tvInfo;

        public ChatOwnViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(final int position) {
            Chat chat = chats.get(position);
            titleMessage.setText(chat.getContent().trim());
            tvInfo.setText(formatRelativeTime(chat.getTime()));
        }
    }

    private String formatRelativeTime(String time) {
        try {
            return DateUtils.getRelativeTimeSpanString(Long.valueOf(time), System.currentTimeMillis(),
                    DateUtils.MINUTE_IN_MILLIS).toString();
        } catch (NumberFormatException e) {
            return time;
        }
    }

    @FunctionalInterface
    interface OnItemClickListener {
        void OnItemClick(int position);
    }
}

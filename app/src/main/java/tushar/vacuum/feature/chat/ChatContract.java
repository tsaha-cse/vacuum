package tushar.vacuum.feature.chat;

import java.util.List;

import tushar.arch.BaseContract;
import tushar.vacuum.model.Chat;

/**
 * Created by TUSHAR on 2/19/18.
 * tsaha.cse@gmail.com
 */

public interface ChatContract {

    interface ChatView extends BaseContract.View {

        void setResult(List<Chat> chats);

        void insertResult(Chat chat);

        void onLogout();
    }

    interface ChatPresenter extends BaseContract.Presenter<ChatView> {

        void loadData(@SuppressWarnings("SameParameterValue") boolean forceRefresh);

        String getUserName();

        void logout();
    }
}

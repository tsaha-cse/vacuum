package tushar.vacuum.feature.chat;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import tushar.arch.BasePresenter;
import tushar.vacuum.di.RunOn;
import tushar.vacuum.model.Chat;
import tushar.vacuum.repository.DataRepository;
import tushar.vacuum.session.Session;

import static tushar.vacuum.di.SchedulerType.IO;
import static tushar.vacuum.di.SchedulerType.UI;

/**
 * Created by TUSHAR on 2/19/18.
 * tsaha.cse@gmail.com
 */

public class ChatPresenter extends BasePresenter<ChatContract.ChatView> implements ChatContract.ChatPresenter {

    private final DataRepository dataRepository;
    private final Scheduler ioScheduler;
    private final Scheduler uiScheduler;
    private final Session session;

    private final CompositeDisposable disposeBucket;

    @Inject
    public ChatPresenter(DataRepository dataRepository, @RunOn(IO) Scheduler ioScheduler,
                         @RunOn(UI) Scheduler uiScheduler, @Named("shared_preference") Session session) {
        this.dataRepository = dataRepository;
        this.ioScheduler = ioScheduler;
        this.uiScheduler = uiScheduler;
        this.session = session;

        disposeBucket = new CompositeDisposable();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onPresent() {
        if (session.isLoggedIn()) {
            getView().showLoading();
            // set true to force update from server
            // in that case chats will be refreshed every time when
            // activity get restarted and local chats
            // will not persist
            loadData(false);
        } else {
            getView().onLogout();
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void onAbsent() {
        disposeBucket.clear();
    }

    @Override
    public void loadData(boolean forceRefresh) {
        Disposable disposable = dataRepository.loadData(forceRefresh)
                .subscribeOn(ioScheduler)
                .observeOn(uiScheduler)
                .subscribe(this::onSuccess, this::onError, () -> getView().hideLoading());
        disposeBucket.add(disposable);
    }

    @Override
    public String getUserName() {
        return session.getUser().getFullName();
    }

    private void onSuccess(List<Chat> chats) {
        getView().hideLoading();
        getView().setResult(chats);
    }

    private void onError(Throwable throwable) {
        getView().hideLoading();
        getView().showToast((throwable instanceof IOException) ? "No Internet Connection" : throwable.getMessage());
    }

    public void onClickSendMessage(String content) {
        Chat ownChatMessage = Chat.build(getUserName(),
                content, String.valueOf(System.currentTimeMillis()));
        Disposable disposable = insertChat(ownChatMessage)
                .subscribeOn(ioScheduler)
                .observeOn(uiScheduler)
                .subscribe(() -> onSuccessInsert(ownChatMessage), this::onError);
        disposeBucket.add(disposable);
    }

    private void onSuccessInsert(Chat ownChatMessage) {
        getView().insertResult(ownChatMessage);
    }

    private Completable insertChat(Chat ownChatMessage) {
        return Completable.fromAction(() -> dataRepository.addData(ownChatMessage));
    }

    @Override
    public void logout() {
        Disposable disposable = Completable.fromAction(() -> {
            session.flush();
            dataRepository.clearData();
        }).subscribeOn(ioScheduler)
                .observeOn(uiScheduler)
                .subscribe(this::onSuccessLogout, this::onError);
        disposeBucket.add(disposable);
    }

    private void onSuccessLogout() {
        getView().onLogout();
    }
}

package tushar.vacuum.feature.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.Toast;

import butterknife.ButterKnife;
import tushar.arch.BaseActivity;
import tushar.arch.BaseContract;
import tushar.vacuum.App;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

public abstract class BaseExtensionActivity<V extends BaseContract.View, P extends BaseContract.Presenter<V>>
        extends BaseActivity<V, P> {

    protected App app;
    private ProgressDialog pd;

    @CallSuper
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        app = (App) getApplication();
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        ButterKnife.bind(this);
        init();
    }

    @LayoutRes
    protected abstract int getLayoutResId();

    protected abstract void init();

    @Override
    public void showLoading() {
        pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    @Override
    public void hideLoading() {
        if (pd != null) {
            pd.dismiss();
        }
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

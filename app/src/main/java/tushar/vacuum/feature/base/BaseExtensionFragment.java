package tushar.vacuum.feature.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import tushar.arch.BaseContract;
import tushar.arch.BaseFragment;
import tushar.vacuum.App;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

@SuppressWarnings("ALL")
public abstract class BaseExtensionFragment<V extends BaseContract.View, P extends BaseContract.Presenter<V>> extends BaseFragment<V, P> {

    protected App app;
    protected ProgressDialog pd;
    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (App) getActivity().getApplication();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(view);
    }

    @Override
    public void showLoading() {
        pd = new ProgressDialog(getActivity());
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    @Override
    public void hideLoading() {
        if (pd != null) {
            pd.dismiss();
        }
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}

package tushar.vacuum.repository.local;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import tushar.vacuum.model.Chat;
import tushar.vacuum.repository.DataSource;
import tushar.vacuum.repository.local.db.ChatDao;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

public class LocalDataSource implements DataSource {

    private final ChatDao chatDao;

    @Inject
    public LocalDataSource(ChatDao chatDao) {
        this.chatDao = chatDao;
    }

    @Override
    public Flowable<List<Chat>> loadData(boolean forceRemote) {
        return chatDao.getAllChat();
    }

    @Override
    public void addData(Chat chat) {
        chatDao.insert(chat);
    }

    @Override
    public void clearData() {
        chatDao.deleteAll();
    }
}

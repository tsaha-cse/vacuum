package tushar.vacuum.repository.local.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import tushar.vacuum.model.Chat;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

@Dao
public interface ChatDao {
    String TABLE_NAME = "chat";

    @Query("SELECT * FROM " + TABLE_NAME)
    Flowable<List<Chat>> getAllChat();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Chat chat);

    @Query("DELETE FROM " + TABLE_NAME)
    void deleteAll();
}

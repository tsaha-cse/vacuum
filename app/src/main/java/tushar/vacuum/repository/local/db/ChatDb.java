package tushar.vacuum.repository.local.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import tushar.vacuum.model.Chat;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

@Database(entities = Chat.class, version = 1, exportSchema = false)
public abstract class ChatDb extends RoomDatabase {
    public static final String DB_NAME = "chat.db";

    public abstract ChatDao chatDao();
}

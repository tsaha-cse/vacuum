package tushar.vacuum.repository;

import java.util.List;

import io.reactivex.Flowable;
import tushar.vacuum.model.Chat;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

public interface DataSource {
    Flowable<List<Chat>> loadData(boolean forceRemote);

    void addData(Chat chat);

    void clearData();
}

package tushar.vacuum.repository.remote;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import tushar.vacuum.api.ApiService;
import tushar.vacuum.api.ChatResponse;
import tushar.vacuum.model.Chat;
import tushar.vacuum.repository.DataSource;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

public class RemoteDataSource implements DataSource {

    private final ApiService apiService;

    @Inject
    public RemoteDataSource(ApiService apiService) {
        this.apiService = apiService;
    }

    @Override
    public Flowable<List<Chat>> loadData(boolean forceRemote) {
        return apiService.getChat().map(ChatResponse::getChats);
    }

    @Override
    public void addData(Chat chat) {
        throw new UnsupportedOperationException("Remote source doesn't support insert operation");
    }

    @Override
    public void clearData() {
        throw new UnsupportedOperationException("Remote source doesn't support delete operation");
    }
}

package tushar.vacuum.repository;

import android.support.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import tushar.vacuum.model.Chat;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

public class DataRepository implements DataSource {

    private final DataSource localSource;
    private final DataSource remoteSource;

    @VisibleForTesting
    List<Chat> cache;

    @Inject
    public DataRepository(@Local DataSource localSource, @Remote DataSource remoteSource) {
        this.localSource = localSource;
        this.remoteSource = remoteSource;
        cache = new ArrayList<>();
    }

    @Override
    public Flowable<List<Chat>> loadData(boolean forceRemote) {
        if (forceRemote) {
            return getFreshData();
        } else {
            if (cache.size() > 0) {
                // cache is available, return it immediately
                return Flowable.just(cache);
            } else {
                return localSource.loadData(false)
                        .take(1)
                        .flatMap(
                                // converting each item to flowable
                                Flowable::fromIterable)
                        .doOnNext(chat -> cache.add(chat))
                        .toList()
                        .toFlowable()
                        .filter(list -> !list.isEmpty())
                        .switchIfEmpty(getFreshData());
            }
        }
    }

    @Override
    public void addData(Chat chat) {
        localSource.addData(chat);
        cache.add(chat);
    }

    @Override
    public void clearData() {
        cache.clear();
        localSource.clearData();
    }

    private Flowable<List<Chat>> getFreshData() {
        return remoteSource
                .loadData(true)
                .doOnNext(chats -> {
                    cache.clear();
                    localSource.clearData();
                })
                .flatMap(
                        // converting each item to flowable
                        Flowable::fromIterable
                )
                .doOnNext(chat -> {
                    // adding to on memory cache
                    cache.add(chat);
                    // adding to db
                    localSource.addData(chat);
                })
                .toList()
                .toFlowable();
    }
}

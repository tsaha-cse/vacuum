package tushar.vacuum.session;

import tushar.vacuum.model.User;

/**
 * Created by TUSHAR on 2/22/18.
 * tsaha.cse@gmail.com
 */

public interface Session {
    boolean isLoggedIn();

    void saveUser(User user);

    User getUser();

    void flush();
}

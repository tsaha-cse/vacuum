package tushar.vacuum.session;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import tushar.vacuum.model.User;

/**
 * Created by TUSHAR on 2/22/18.
 * tsaha.cse@gmail.com
 */

public class SharedPreferenceSessionImpl implements Session {
    private static final String KEY_USER = "user";

    private final SharedPreferences sharedPreferences;
    private final Gson gson;
    private User user;

    public SharedPreferenceSessionImpl(SharedPreferences sharedPreferences, Gson gson) {
        this.sharedPreferences = sharedPreferences;
        this.gson = gson;
    }

    @Override
    public boolean isLoggedIn() {
        return getUser() != null;
    }

    @Override
    public void saveUser(User user) {
        putString(KEY_USER, gson.toJson(user));
        this.user = user;
    }

    @Override
    public User getUser() {
        if (user == null) {
            user = gson.fromJson(getString(KEY_USER), User.class);
        }
        return user;
    }

    @Override
    public void flush() {
        removeKey(KEY_USER);
        user = null;
    }

    private void putString(@SuppressWarnings("SameParameterValue") String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value).apply();
    }

    private String getString(@SuppressWarnings("SameParameterValue") String key) {
        return sharedPreferences.getString(key, null);
    }

    private void removeKey(@SuppressWarnings("SameParameterValue") String key) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key).apply();
    }
}

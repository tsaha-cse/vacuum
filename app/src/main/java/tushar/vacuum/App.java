package tushar.vacuum;

import android.app.Application;

import tushar.vacuum.di.AppComponent;
import tushar.vacuum.di.AppModule;
import tushar.vacuum.di.DaggerAppComponent;

/**
 * Created by TUSHAR on 2/16/18.
 * tsaha.cse@gmail.com
 */

public class App extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}

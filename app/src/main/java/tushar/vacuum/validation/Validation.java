package tushar.vacuum.validation;

/**
 * Created by TUSHAR on 2/22/18.
 * tsaha.cse@gmail.com
 */

public interface Validation {

    boolean isEmpty(String fullName);

    boolean isNameValid(String fullName);

    boolean isNameAvailableForSignUp(String fullName);

    boolean isNullOrEmpty(CharSequence cs);
}

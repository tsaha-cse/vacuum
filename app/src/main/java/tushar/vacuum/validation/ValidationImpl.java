package tushar.vacuum.validation;

import java.util.regex.Pattern;

/**
 * Created by TUSHAR on 2/22/18.
 * tsaha.cse@gmail.com
 */

public class ValidationImpl implements Validation {

    @Override
    public boolean isEmpty(String fullName) {
        return isNullOrEmpty(fullName);
    }

    @Override
    public boolean isNameValid(String fullName) {
        return Pattern
                .compile("([A-Za-z]+\\s+['A-Za-z]).{0,50}", Pattern.CASE_INSENSITIVE)
                .matcher(fullName)
                .matches();
    }

    @Override
    public boolean isNameAvailableForSignUp(String fullName) {
        String[] existNames = {"Carrie", "Anthony", "Eleanor", "Rodney", "Oliva", "Merve", "Lily"};
        for (String existName : existNames) {
            if (existName.equalsIgnoreCase(fullName.trim())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isNullOrEmpty(CharSequence cs) {
        return cs == null || isEmpty(cs);
    }

    private boolean isEmpty(CharSequence cs) {
        return cs.length() == 0;
    }
}

package tushar.vacuum.di;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static tushar.vacuum.di.SchedulerType.IO;
import static tushar.vacuum.di.SchedulerType.UI;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

@Module
public class SchedulerModule {

    @Provides
    @RunOn(IO)
    Scheduler provideIo() {
        return Schedulers.io();
    }

    @Provides
    @RunOn(UI)
    Scheduler provideUi() {
        return AndroidSchedulers.mainThread();
    }
}
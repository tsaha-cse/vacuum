package tushar.vacuum.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import tushar.vacuum.repository.DataSource;
import tushar.vacuum.repository.Local;
import tushar.vacuum.repository.Remote;
import tushar.vacuum.repository.local.LocalDataSource;
import tushar.vacuum.repository.remote.RemoteDataSource;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

@Module
public class DataSourceModule {

    @Provides
    @Local
    @Singleton
    public DataSource provideLocalDataSource(LocalDataSource localDataSource) {
        return localDataSource;
    }

    @Provides
    @Remote
    @Singleton
    public DataSource provideRemoteDataSource(RemoteDataSource remoteDataSource) {
        return remoteDataSource;
    }
}

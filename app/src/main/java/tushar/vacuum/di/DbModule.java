package tushar.vacuum.di;

import android.arch.persistence.room.Room;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import tushar.vacuum.repository.local.db.ChatDao;
import tushar.vacuum.repository.local.db.ChatDb;
import tushar.vacuum.App;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

@Module
public class DbModule {
    private static final String DATABASE = "database_name";

    @Provides
    @Named(DATABASE)
    String provideDatabaseName() {
        return ChatDb.DB_NAME;
    }

    @Provides
    @Singleton
    ChatDb provideUserDao(App app, @Named(DATABASE) String dbName) {
        return Room.databaseBuilder(app, ChatDb.class, dbName).build();
    }

    @Provides
    @Singleton
    ChatDao provideQuestionDao(ChatDb chatDb) {
        return chatDb.chatDao();
    }
}

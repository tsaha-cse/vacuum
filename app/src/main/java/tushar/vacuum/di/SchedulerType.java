package tushar.vacuum.di;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

public enum SchedulerType {
    IO, UI
}

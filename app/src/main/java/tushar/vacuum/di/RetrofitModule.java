package tushar.vacuum.di;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import tushar.vacuum.api.ApiService;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

@Module
public class RetrofitModule {

    @Named("timeout_session")
    @Provides
    @Singleton
    int getTimeOutDuration() {
        return 30;
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor,
                                     @Named("timeout_session") int timeoutSession) {
        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder();
        okhttpClientBuilder.addInterceptor(loggingInterceptor);
        okhttpClientBuilder.connectTimeout(timeoutSession, TimeUnit.SECONDS);
        okhttpClientBuilder.readTimeout(timeoutSession, TimeUnit.SECONDS);
        okhttpClientBuilder.writeTimeout(timeoutSession, TimeUnit.SECONDS);
        return okhttpClientBuilder.build();
    }

    @Named("api_url")
    @Provides
    @Singleton
    String getApiUrl() {
        return ApiService.URL;
    }

    @Provides
    @Singleton
    Converter.Factory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @Singleton
    CallAdapter.Factory provideRxJavaAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(@Named("api_url") String url, OkHttpClient okHttpClient, Converter.Factory gsonConverter,
                             CallAdapter.Factory rxCallAdapter) {
        return new Retrofit.Builder()
                .baseUrl(url)
                .client(okHttpClient)
                .addConverterFactory(gsonConverter)
                .addCallAdapterFactory(rxCallAdapter)
                .build();
    }

    @Provides
    @Singleton
    ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }
}

package tushar.vacuum.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import tushar.vacuum.App;
import tushar.vacuum.validation.Validation;
import tushar.vacuum.validation.ValidationImpl;

/**
 * Created by TUSHAR on 2/16/18.
 * tsaha.cse@gmail.com
 */

@Module
public class AppModule {
    private final Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    App provideApp() {
        return (App) application;
    }

    @Provides
    @Singleton
    Validation provideValidation() {
        return new ValidationImpl();
    }
}

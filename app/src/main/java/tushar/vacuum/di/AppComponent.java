package tushar.vacuum.di;

import javax.inject.Singleton;

import dagger.Component;
import tushar.vacuum.di.chat.ChatComponent;
import tushar.vacuum.di.chat.ChatModule;
import tushar.vacuum.di.login.LoginComponent;
import tushar.vacuum.di.login.LoginModule;

/**
 * Created by TUSHAR on 2/16/18.
 * tsaha.cse@gmail.com
 */

@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class, DbModule.class,
        DataSourceModule.class, SchedulerModule.class, SessionModule.class})
public interface AppComponent {
    LoginComponent plus(LoginModule module);

    ChatComponent plus(ChatModule module);
}

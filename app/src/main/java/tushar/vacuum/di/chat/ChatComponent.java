package tushar.vacuum.di.chat;

import javax.inject.Singleton;

import dagger.Subcomponent;
import tushar.vacuum.feature.chat.ChatActivity;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

@Singleton
@Subcomponent(modules = ChatModule.class)
public interface ChatComponent {
    void inject(ChatActivity activity);
}

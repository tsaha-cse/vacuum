package tushar.vacuum.di;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import tushar.vacuum.App;
import tushar.vacuum.session.Session;
import tushar.vacuum.session.SharedPreferenceSessionImpl;

/**
 * Created by TUSHAR on 2/22/18.
 * tsaha.cse@gmail.com
 */

@Module
public class SessionModule {

    @Provides
    @Singleton
    SharedPreferences provideSharedPreference(App app) {
        return PreferenceManager.getDefaultSharedPreferences(app);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new Gson();
    }

    @Named("shared_preference")
    @Provides
    @Singleton
    Session provideSession(SharedPreferences preferences, Gson gson) {
        return new SharedPreferenceSessionImpl(preferences, gson);
    }
}

package tushar.vacuum.di.login;

import javax.inject.Singleton;

import dagger.Subcomponent;
import tushar.vacuum.feature.login.LoginActivity;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

@Singleton
@Subcomponent(modules = LoginModule.class)
public interface LoginComponent {
    void inject(LoginActivity activity);
}

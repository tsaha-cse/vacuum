package tushar.vacuum.model;

/**
 * Created by TUSHAR on 2/21/18.
 * tsaha.cse@gmail.com
 */

public enum UserType {
    OWN(1),
    OTHER(2);

    private int type = 2; // default OTHER

    UserType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}

package tushar.vacuum.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import tushar.vacuum.repository.local.db.ChatDao;

@Entity(tableName = ChatDao.TABLE_NAME)
public class Chat {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "user_type")
    private int userType = UserType.OTHER.getType();

    @Expose
    @SerializedName("username")
    @ColumnInfo(name = "name")
    private String username;

    @Expose
    @SerializedName("content")
    @ColumnInfo(name = "content")
    private String content;

    @Expose
    @SerializedName("userImage_url")
    @ColumnInfo(name = "image_url")
    private String userImageUrl = "";

    @Expose
    @SerializedName("time")
    @ColumnInfo(name = "time")
    private String time;

    /**
     * build OWN type chat message
     */
    public static Chat build(String username, String content, String time) {
        Chat chat = new Chat();
        chat.setUserType(UserType.OWN.getType());
        chat.setUsername(username);
        chat.setContent(content);
        chat.setTime(time);
        return chat;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
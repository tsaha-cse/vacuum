package tushar.vacuum.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

/**
 * Created by TUSHAR on 2/22/18.
 * tsaha.cse@gmail.com
 */

public final class User {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("full_name")
    private String fullName;

    @Expose
    @SerializedName("created_at")
    private long createdAt;

    public static User build(String fullName) {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setFullName(fullName);
        user.setCreatedAt(System.currentTimeMillis());
        return user;
    }

    public String getId() {
        return id;
    }

    private void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    private void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    private void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }
}

package tushar.vacuum.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import tushar.vacuum.model.Chat;

public class ChatResponse {

    @SerializedName("chats")
    private List<Chat> chats = null;

    public List<Chat> getChats() {
        return chats;
    }
}

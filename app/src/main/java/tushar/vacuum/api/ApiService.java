package tushar.vacuum.api;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import tushar.vacuum.BuildConfig;

/**
 * Created by TUSHAR on 2/17/18.
 * tsaha.cse@gmail.com
 */

public interface ApiService {
    String URL = BuildConfig.API_URL;

    @GET("rocket-interview/chat.json")
    Flowable<ChatResponse> getChat();
}

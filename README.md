# Vacuum-Nobody is there to listen you! #

## Project Setup ##

### Architecture ###
- Used MVP+Android Architecture Component (LifeCycle+ViewModel) to make Presenter Lifecycle-aware which solves problems with configuration changes, support data percistency and prevent memory leak.
- RxJava+RxAndroid is used to make process asynchonous.
- ':arch' module represents the architecture module. 

### Network ###
- Used Retrofit2+RxJavaAdapter+GSON

### Storage ###
- Used SharedPreference for creating an User
- Used Room DB for saving chats for persistance

### Dependency Injecttion ###
- Dagger2
- View Injection: ButterKnife

### Image ###
- Picasso for image downloading
- RoundedImageView for showing avatar shape profile photo

### Unit Test ###
- Junit
- Mockito
- Hamcrest

## UX ##
- After a fresh installation: A simple login page asking for 'Enter your full name' will be appeared. You are open to enter any name except the reserve words. You will need to put both first and last name separated by a write space to pass the validation. You will enter to the chat screen if validation is passed.
- For the first time landing on the chat screen app will requre a network call to get the conversation from the given API. This API call is once for each login.
- The chats are kept saved in Room DB. So when you are already logged in and you enter to app, you will get all of the chat history without any API call.
- You are allowed to type and chat here (btw, no one will reply). The texts you 'type and enter' will also be kept saved in Room DB for persistence.
- You will lose all of your chats while you logged out. Find logout button in toolbar of the chat screen.

## Test Process ##
- Device: Huawei P10, Android 7.0
- Unit Test implemented
- Android Lint: Solved suspected bugs and Performance issues

## APK ##
[chat.apk](apk/chat.apk)


